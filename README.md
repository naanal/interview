## Using Rest APIs, create frontend applications

**General Tips**

1.  You can use any design framework like Bootstrap, Foundation, Material along with angular or React JS
2.  Use [hurl](https://www.hurl.it/) or [resttest](https://resttesttest.com/) or [POSTMAN](https://www.getpostman.com/) to play with APIs
3.  You are free to use Internet
4.  Use online code editor like [plnkr](plnkr.co), [jsfiddle](https://jsfiddle.net/), [codepen](https://codepen.io/), [jsbin](http://jsbin.com), [playcode](http://playcode.io) or [expo](https://snack.expo.io/) in case of mobile app

### Level 1:  (Time: 30 Minutes)

**Task 1: Find Country by IP address**

_REST API:_

` GET  https://api.ip2country.info/ip?5.6.7.8`

_Frontend:_

1. Create Textbox in which IP address can enter. Check whether IP address is valid or not.
2. Create 'Check Country' Button. Enable Button only if IP address is valid
3. If the button is clicked call the api to get the country detail and show it

**Task 2: Find Gender by Name**

`GET https://api.genderize.io/?name[0]=raja&name[1]=peter&name[2]=yazhini`

_Frontend:_

1. Create Texbox in which multiple names can enter separated by comma
2. Create 'Find Gender' Button.
3. If the Button is clicked parse the names and 'form API query url' and send it to API
4. Get the response and show the gender

***

### Level 2: (Time: 45 Minutes)

**Task: CRUD Operation**

Sample APP: http://movieapp-sitepointdemos.rhcloud.com

Host:  http://movieapp-sitepointdemos.rhcloud.com

| URL              | HTTP METHOD   | POST BODY   | RESULT                |
| ---------------- | ------------- | ----------- | --------------------- | 
| /api/movies      | GET           | empty       | Return all movies     |
| /api/movies      | POST          | JSON String | New Movie Created     |
| /api/movies/:id  | GET           | empty       | Returns single movie  |
| /api/movies/:id  | PUT           | JSON String | Update existing movie |
| /api/movies/:id  | DELETE        | empty       | Delete a movie        |

_REST APIs:_

**` GET http://movieapp-sitepointdemos.rhcloud.com/api/movies`**

_Response_

	[
		{
		__v: 0,
		_id: 32596,
		director: "Director Name 1 ",
		genre: "Thriller",
		releaseYear: "1990",
		title: "Movie Name 1"
		},
        {
		__v: 0,
		_id: 32597,
		director: "Director Name 2 ",
		genre: "Horror",
		releaseYear: "2002",
		title: "Movie Name 2"
		}
	]

**` POST http://movieapp-sitepointdemos.rhcloud.com/api/movies`**

_Request_

	{ 
		"title": "Move Title", 
		"releaseYear": "1990", 
		"director": "Director Name", 
		"genre": "Horror"
	}

_Response_

       {
                "message": "New Movie Created",
       }

**` PUT http://movieapp-sitepointdemos.rhcloud.com/api/movies/32598`**

_Request_

	{
		"title": "New Title",
		"releaseYear": "2017",
		"director": "New Director",
		"genre": "New Genre"
	}

_Respose_
  
	{
		"message": "Movie updated!"
	}

**` DELETE http://movieapp-sitepointdemos.rhcloud.com/api/movies/32598`**

	{
		"message": "Successfully deleted"
	}

***

### Level 3: (Time: 45 Minutes)

**Task: Create Courier Order Tracking Application**

1. Create dialogue box in which new tracking can be added. Create Textbox (for tracking number), Dropdown (for courier) and ADD Button
2. List all of your tracking with tracking information. 


For All request pass the below Headers in your API call:

	aftership-api-key    e18d3213-0fc9-469b-9a53-fe982f410399
	Content-Type         application/json


**To Get the Couriers**

**`GET https://api.aftership.com/v4/couriers/`**

**To Add New Tracking Order**

** `POST https://api.aftership.com/v4/trackings` **

Request Payload/ Body

	{
		"tracking": {
			"tracking_number":"950475403",
			"slug": "gati-kwe"
		}
	}

** To Get All of your Tracking **

** `GET https://api.aftership.com/v4/trackings/` **


For more information, Follow the Doc: https://www.aftership.com/docs/api/4/overview

***

### Level 4: (Time: 1 Hour)

**Task: GIF Gallery like pinterest**

` GET  http://api.giphy.com/v1/gifs/search?q=:searchterm&api_key=723b58c6b917462ba8da20223ee76acc `
It returns 25 results.

` GET  http://api.giphy.com/v1/gifs/search?q=:searchterm&api_key=723b58c6b917462ba8da20223ee76acc&offset=25`
It returns next 25 results.

1. Create Search Box 
2. Enter Search Term and click search
3. Display all GIFs in masonry layout
4. While User hits bottom of the screen call next list of images using offset and append it to screen.