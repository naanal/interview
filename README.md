**Task 1: Find Country by IP address**

GET https://api.ip2country.info/ip?5.6.7.8

*Inputs:*
<br/>

1. IP address 

*Functionalities:*

1. Create Textbox in which IP address can enter. Check whether IP address is valid or not.
2. Create 'Check Country' Button. Enable Button only if IP address is valid.
3. If the button is clicked call the api to get the country detail and show it.



**Task 2:  CRUD operation**

Create a phonebook web app

*Inputs:*
<br/>
1. 	First Name
2. 	Last Name
3. 	Phone Number

*Functionalities:*

1. Option to create new contact.
2. List all contacts.
3. Edit a exisitng contact.
4. Delete a contact with confirmation dialog.
5. Search option, in which user can search either by name or phone number.